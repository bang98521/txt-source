根據春雪後來聽到的說法，被留在中城大樓北側公園的黑雪公主等人稍微花了一些時間，才讓所有人開始採取接下來的行動。

春雪起飛去追擄走Scarlet　Rain的Black　Vice之際，留下了這麼兩句話。「Pard小姐請去追Argon！」「哪個人從最近的傳送門脫離，去拔掉仁子的傳輸線！」

最先衝出去的是Blood　Leopard。她為了追擊從遠方大樓屋頂以雷射掩護Vice的「四眼分析者」Argon　Array，開啟野獸模式全力飛奔，轉眼間就不見踪影。

留在公園的有黑雪公主、楓子、晶、謠、拓武與千百合等六人。但謠被Argon以四發雷射貫穿胸部，當場失去行動能力，被楓子抱在懷裡。眾人才剛會合，千百合就以香櫞鐘聲幫她恢復體力計量表，但要擺脫重傷的痛覺震撼多半還得花上一些時間。於是眾人必須兵分兩路，一隊前件支援Leopard，另一隊則前往傳送門，以便停止Rain的加速。

當黑雪公主想到這裡，公園北側便響起大規模的爆炸聲響。放眼望去，只見白堊街景的一角冒出通紅的火柱。眾人一陣緊張，以為Leopard遭到迎擊，但晶立刻輕聲說：

「不用擔心，那是Pard的招式。」

「是嗎？那麼——」

黑雪公主拉回視線……

「——Pile、Bell，麻煩你們兩個去追Leopard!」

她犀利地一聲令下，拓武與千百合不約而同點頭回答：

「包在我們身上，學姊！」「了解，軍團長。」

兩人極有默契地回答完後往北跑去。黑雪公主也不目送他們離開，轉向楓子與晶繼續說：

「Raker、Current，我們進大樓！」

最靠近的傳送門所在的位置，位於加速研究社重要據點所在的中城大樓內部。雖然已經排除了本是最大障礙的大天使梅丹佐，但根本不知道裡面還有什麼樣的圈套與強敵等著他們。

但兩名老資格的玩家並未立刻點頭。Raker一對晚霞色的鏡頭眼上露出關心的神色，用很快的連度說：

「Lotus，不去幫鴉同學沒關係嗎？照那樣下去，即使他追上Vice，也會演變成一對一的戰鬥。」

晶失去的大部分流水裝甲仍未恢復，對此也點頭表示同意。但黑雪公主仰望著滿天晚霞的天空中留下的一道軌跡，搖搖頭說：

「沒關係。我相信Crow……相信我的下輩。而且你們也看到他那無與倫比的飛行速度了吧？憑現在的他，即使一對一也打得贏Vice！」

黑雪公主斬釘截鐵地這麼一說，兩人也深深點了點頭。接著被楓子抱在懷裡的謠也發出了細小的聲音：

「就是……這樣。當鴉鴉為了，重要的，朋友而戰……他就，所向無敵。」

「小梅！你還好嗎？」

聽楓子這麼問，嬌小的巫女堅強地回答：

「我已經沒事了。會眼睜睜看著Rain，被搶走……是我的責任，我沒能注意到，Argon的攻擊。我不能，沒完沒了地，要人抱著。」

說完謠就自行下到地上。在痛覺擴大到兩倍的無限制空間裡，被四發超高熱雷射同時擊中的痛覺震撼應該還留在身上，但她的腳步卻讓人看不出這種跡象。

演變成這種事態的責任，不可能完全歸咎在謠一個人身上，但黑雪公主為了回應謠的覺悟，特意點頭回答：

「Maiden，你還能打吧？」

「那當然！」

「好。那我們四個人就從現在開始突入中城大樓，記得傳送門的位置是……」

晶立刻幫黑雪公主補充：

「如果跟以前一樣，應該會在四十五樓。」

四人同時仰望的這座白堊巨塔，從屋頂到接近地上的樓層之間有著一道垂直的裂痕。是Silver　Crow以特殊能力「光學傳導」折射梅丹佐的超高威力雷射，將大樓劈成了兩半。儘管無論從這道寬約兩公尺的裂痕如何凝神細看，仍然看不見傳送門的藍光，但只要從這道縫隙直接進入四十五樓，就能大幅縮減所需的時間。

「……大概兩百公尺出頭吧。Raker……」

黑雪公主轉過身去，楓子就輕輕搖了搖頭。

「很遺憾，憑我的疾風推進器，沒有辦法把你們三人載到那樣的高度。看是要我帶一個人先走，還是四個人一起飛，能飛到几樓就是几樓……」

「嗯……」

黑雪公主在眨了一次眼睛的空檔中做出了決定。

「我們四個一起走。強制斷線是救出Rain的最終手段，無論如何都不容許失敗。相信Crow一定會幫我們爭取上樓的時間。」

「也對。我也會卯足現在的全力去飛。」

楓子這麼一宣告，就大大攤開雙手。謠抱在她身上，黑雪公主與晶則分別抓住她的左右手。裝備在背上的整個推進器開始發出淡淡的藍色光芒，高亢的驅動聲響起，液態金屬狀的長髮張開成有如翅膀般的形狀。

「……我要飛了！」

楓子呼喊的同時，雙腿用力往地上一蹬。

緊接著疾風推進器的噴嘴迸出蒼藍的火焰，四人有如火箭一般開始朝巨塔的高樓層飛去。

***

——絕對、絕對。

——我絕對，要保護她。

化為一陣深紅色疾風的Blood　Leopard——掛居美早，腦中一再重覆著這句話。

她並非完全沒料到加速研究社會出手攻擊。紅之團的大批團員在無限制空間展開行動的過程中，遭到化身為黑之王Black　Lotus模樣的對戰虛擬角色驅策神獸級公敵襲擊，還只是兩天前的事情。

假Lotus的真面目，就是自稱加速研究社副社長的Black　Vice。奇襲的目的，則是偵察日珥與Scarlet　Rain這些情報紅之王都已經在昨天告訴了她。雖然不太確定Vice假冒成黑之王的理由，但美早與仁子推測，多半是為了讓這兩個處於無限期停戰的軍團反目。而在星期六的領土戰爭中，也確實有三名日珥的團員擅自去攻打杉並戰區。

但事情發展到這一步，愈想就愈覺得實際理由正好相反。

只要化身為黑之王的模樣攻擊紅之王，引發日珥的報復行動，兩軍團之間就會進行首腦層級的會談以便收拾事態。然後一旦在這個時間點上，有黑之團的團員，或與他們有著密切關係的超頻連線者——舉例來說，就像Ash　Roller——被ISS套件感染，相信重情義的紅之王就會提議要幫忙。

而在梅丹佐之前就先和他們交戰的Magenta　Scissor，被問到盯上Ash　Roller的理由時，就回答說是因為「有很多事情要顧」。而她所謂的有很多事情要顧，很可能就是指加速研究社指示她這麼做。這一切都是為了在無限制空間擄走紅之王Scarlet　Rain而布的局。

要說這是計劃，不確定因素未免太多。然而這多半正是加速研究社的作風。他們會接二連三在加速世界中灑下災禍的種子，然後收割偶然結成的果實。像Rain的「上輩」Cherry　Rook被災禍之鎧上身時、秋葉原BG遭到Rust　Jigsaw蹂躪時，還有Silver　Crow的翅膀被Dusk　Taker舍走時，損害本來都可能再大上好幾倍。不，說不定研究社的圖謀已經引發了無數莫大的悲劇，只是美早不知道而已。

——可是，只有現在，現在萬萬不能讓這些傢伙稱心如意。

很遺憾的，美早沒有能力去追把Rain關住而沉入影子當中的Black　Vice箱但他那邊有成長卓著的Silver　Crow用甚至可能超越音速的驚人飛行速度追趕。美早該做的事，是抓住掩護Vice的Argon　Array。Crow之所以指示她這麼做，多半是考慮到要拿Argon來當和紅之王交換用的人質。

Argon發射雷射的大樓，距離中城大樓有三百公尺以上。就連戰區邊界上設有障壁的正規對戰空間裡，這樣的距離都難以在短時間跑完，更何況這裡還是沒有邊界的無限制中立空間。然而……

——別想逃走！

美早從豹的血盆大口迸出灌注了這個意志的咆哮，猛力往地面一蹬。

Argon的身影從她跑向的大樓屋頂離開，已經過了十秒鐘以上。萬萬不能讓她和Black　Vice會合。儘管已經發動能以時速兩百公里奔跑的特殊能力「第一滴血」，但還是不夠。

Blood　Leopard已經很長一段時間沒動用她最強最快的5級必殺技，而現在時候已經到了。

——瞄準。目標鎖定在前方兩百公尺的五層樓建築。

——裝填。高高跳起，收疊手腳。四周有紅光構成管子，也就是砲管。

然後——點火。

「『流血砲擊』（Bloodshad Canon）！」

喊出招式名稱的同疇，必殺技計量表剩下的部分幾乎全部耗盡，美早的身體在一聲撼動天地的巨響中化為一顆砲彈，從半透明的砲管發射出去。視野消融成放射狀，變得一片全白。短短一秒鐘後，傳來一陣幾乎把全身骨頭都拆了似的衝擊，讓美早用力咬緊牙關。

體力計量表瞬間減少三成以上，裝甲到處出現龜裂。視野恢復正常後，看見的是在橘色爆炸火焰背景中飛散的無數白色碎片——是美早瞄準的整棟建築物被「砲擊」炸得粉碎的光景。

日珥副團長Blood　Loepard以身為違反加速世界定律的「紅色純近戰型」角色而知名。因為她的虛擬角色明明有著彩度相當高的遠攻紅色，卻完全不使用遠距離攻擊。就連紅之團的團員，也幾乎全都相信她那「血腥小貓」的綽號，是來自她以尖銳的爪與牙撕裂敵人，連藍色系虛擬角色看了都會汗顏的凶猛戰法。

但其實不是這樣。她會弄得渾身是血，並不是因為濺到敵人身上噴出來的血，而是因為動用幾近於自爆的必殺技「流血砲擊」而毀損自己的裝甲。這一招威力強大，只要能夠命中，幾乎足以讓任何對戰虛擬角色當場死亡。然而一旦落空，撞上建築物或地面，死的就會是自己。這次她命中大型建築物，體力計量表卻只減少三成，是因為這棟建築物在黃昏屬性下變得脆弱得多。也曾有人從這大砲的概念，看出她虛擬角色名稱中的Leopard，是取自現實申存在的戰車名稱。

但美早對這「流血砲擊」只用了一陣子就再也不動用，之後完全以近戰型的打法奮戰到今天。理由主要有兩個。首先，這一招中了就打贏、沒中就打輸。要賺取到足夠的剩餘點數來承受四神青龍的等級吸收攻擊，就不能依賴這種賭博性質太重的招式。第二則是因為她領悟到只要自己一天繼續依賴這種不夠可靠的力量，就永遠也追不上把疾風推進器這種與「流血砲擊」相似卻又大不相同的能力完全駕馭自如的Sky　Raker。

就連升上8級的現在，她仍然感受到自己的道行還遠遠不足。然而這次並不需要精確的瞄準，而且也不是保留實力的時候了。她必須動員所有的力量與手段捉住Argon　Array。

美早心中燃燒著這份決心，從自己引起的大爆炸中心，將視線從右往左掃過一遍。

接著她在飛散的大量土石中看見了，看見一百公尺外的暗處產出了紫光。錯不了，那是Argon的裝甲反射出來的光。

——再來一次！

美早在空中縮起手腳，才剛讓虛擬身體固定成砲彈形，就喊出招式名稱。「流血砲擊」會消耗大量的必殺技計量表，但既然以爆炸炸毀了整棟建築物，也就能夠得到大量的破壞物件加成。只要體力計量表撐得住，就能夠連續發動。

美早在爆響中往斜下方「發射」出去，轉眼間就衝進錯綜複雜的巷道，貫穿兩棟較小的建築物之後著彈。這次引起的大爆炸，將周圍化為一片斷垣殘壁的汪洋。體力計量表再度減少，降低到五成以下，但這次敵人應該也並非毫髮蕪傷。

她在空中翻了個筋斗，落到地上的同時，視野也恢復正常。她睜大的雙眼捕捉到的，就是Argon在她前方交叉雙手擺出防御架式來抵御爆炸的身影。

「嘎嗚！」

美早以豹的血盆大口迸出憤怒的咆哮，一躍而上。

Argon　Array就如她的外號「四眼分析者」所示，屬於收集資訊型的對戰虛擬角色。她的主要能力是以大型鏡頭眼所具備的透視能力來掃瞄其他虛擬角色，不只是能力，就連物品欄的內容都能得知，但自身的戰鬥能力偏低——就連六大軍團的幹部，應該也是這麼認為。

但這只是幌子。她不但隱瞞自己是加速研究社幹部的事實，對自己的戰鬥力也深藏不露。她的雙眼與帽子上合計四個大型鏡頭，不是只能發射無害的透視光線，也可以發射連金屬色角色的裝甲都能瞬間貫穿的超高溫超高速雷射攻擊。

交叉防御的雙手後方唯一露出的一顆鏡頭，發出鮮明的紫色光芒。這次不是反射夕陽的光線，而是發射雷射的前兆。亮度瞬間大增的光線凝眾成一個點，發出十字的閃光。在這個距離下是不可能閃開的。

但美早無視於左肩被雷射貫穿的劇痛，就這麼撲向Argon體力計量表又被削減一整段，進入紅色危險區域，但她以用牙齒咬住敵人為最優先目標。首先撲上去撞得Argon失去平衡，繞到她身後，再猛力咬上她左唇。

四根獠牙穿破薄弱的裝甲，灑出紅色的損傷特效光芒。

「痛痛痛！等等，這樣會痛啦！」

Argon一邊呼痛，一邊用雙手想掙脫美早，但豹的下顎並未軟弱到比力氣會比輸非近戰型角色。分析者看出無法掙脫，於是盡可能把頭往左轉想對她發射雷射，但鏡頭的角度剛好捕捉不到美早。

美早的體力計量表隨著紅光的脈動而開始恢復。這是只有在野獸模式下可以使用的特殊能力「奪活咬」的效果。儘管不會顯示在視野中，但相信Argon的體力應該正以同樣的速度減少。從身後咬住對方不放，是美早的必勝套路，只有極少數對戰虛擬角色曾經從這種狀況下脫身。

「痛痛痛……真是的，這工作哪裡輕鬆了啦！說什麼只要射一發再跑掉就好，要跑掉也太難了吧！」

相信這句指責不是針對美早，而是針對Black　Vice。看來Vice早就已經從中城大樓附近逃脫，但追去的Silver　Crow聲息也跟著消失，所以應該追踪成功了——美早很想這麼相信。

如果Crow能就這麼打倒Vice救回紅之王，自然是最好的結果，但實在不能讓還只有5級的他背負這樣的重責。當然美早也花非悲觀認為他一定會打輸，但她自己的工作就是捉住Argon以做為交換用的人質。要達到這個目的，眼前最實際的方法，就是把對方的計量表消耗完。由於美早在完成Aqua　Current救出作戰後就立刻升上8級，等級和Argon一樣，所以即使打倒Argon，能搶到的點數也只有10點，但能讓對方陷入幽靈狀態，讓座標被定在這裡六十分鐘卻是很大的好處。

美早秉持毫不留情的決心，卯足全力始終咬住Argon細嫩的脖子不放。在無限制空間裡要害持續受創，應該會讓Argon感受到極劇烈的疼痛，但比起過去因為加速研究社的陰謀而犧牲的超頻連線者所受到的痛苦總量，根本就不算什麼。

「啊，糟糕……我頭開始昏了。嗯！這，有點不是……開玩笑啊。至少換個地方咬嘛……只是這話說了也是白說啊。」

Argon說話的口氣還是一樣吃定人，但也變得斷斷續續。美早無法說話，只發出低吼聲回應。她的體力計量表已經恢復到將近七成，只要這樣的狀況持續下去，相信再過不了多久Argon就會死亡。

「唉，沒辦法。畢竟小貓咪都秀出這麼超大手筆的底牌了，我也……不能再吝嗇了呢。」

「…………！」

Argon的話讓美早綳緊了神經。雷射的射線已經捕捉不到美早，在這種狀況下，Argon應該已經沒有任何手段能夠反敗為勝。也許只是虛張聲勢……還是說，她真的另有奇策？

就在這時，美早背上竄過一陣尖銳的戰慄。

現在最應該優先的不是用「奪活咬」完全恢復體力計量表，而是立刻要了她的命。美早本能地判斷出這一點，舉起右手想用鈎爪撕開ArgonH門大開的背。

但就在下一瞬間——

從趴在地上的纖細虛擬角色全身迸射出了鮮紅的堇色光芒。這不是必殺技，而是過剩光，心念系統發動的證明——

「『無線陣列』（Infinite Array）。」

當耳裡聽到這句輕聲細語說出的招式名稱時，想像已經凝聚完成。發動所需的時間只有短短的零點五秒，美早完全沒有時間構思對策，本能地將牙齒從Argon脖子上放開想往後跳離。

但是已經太遲了。

「分析者」身上所有裝甲的表層，都產生了小型的鏡頭。無數排列得井然有序的眼睛醞釀出十字形的光芒——

嗡。

無數道極細的雷射撼動空氣，往全方位發射。這些雷射之中的六十%在地面堆起的土石或幸免於爆炸的建築物上打出黑色的小孔，三十五%呈放射狀射向天空——而剩下的五%則貫穿了美早身上多處。

「嗚…………！」

首先是一陣輕微的衝擊，接著是強烈的灼熱感，最後來到的則是一陣令人暈眩的劇痛，讓美早翻了個筋斗倒在地上。好不容易快要補滿的體力計量表又陷入紅色危險區域，但她沒有時間去精確掌握剩餘的量，用豹的四肢拚命抓著地面試圖站起。要是再挨一次同樣的攻擊，就肯定會沒命。

但也不知道是這種心念攻擊無法連續發動，還是Argon　Array特意要表現得老神在在，只見她慢慢起身說道：

「唉～好痛喔。我也當BB玩家當了很久，這還是第一次不是被公敵，而是差點被對戰虛擬角色咬死呢。」

Argon轉過身來看著美早時，裝甲上仍然還有著大量的「眼睛」，籠罩住她全身的淡淡過剩光也並未消失。無論發動速度、威力、射程、持續時間，全都達到了驚世駭俗的高水準。同屬第四象限——「範圍型破壞心念」的招式，卻幾乎全方位凌駕在過去曾于赫密斯之索縱貫賽中肆虐的Rust　Jigsaw那招「銹蝕秩序」之上。

美早也並非對心念全無素養，但她終究只是為了保護自身不受敵人的心念攻擊傷害而學，如果要比較純粹的破壞力，老實說多半遠遠不及Argon　Argon這招「無限陣列」就是強得這麼過火。不，甚至應該說強得異常，讓美早比都沒比過都能如此斷定。

所有心念都是以使用者的「精神創傷」做為能量來源。創傷是缺陷、是飢渴、是絕望。因此哪怕是從創傷中創造出希望，升華成第一或第二象限的力量，也就是「正向心念」，實際的招式效果仍會出現偏差。追來威力就會失去速度、追求射程就會失去准度，追求範圍就會失去持續力，原理上不可能有所謂萬能的心念。

然而Argon　Array所用的招式，卻不存在任何缺點。

是她花了漫長的時間將招式磨練到這個境界……還是說……

美早一邊思考，一邊勉力想讓受創的身體站起，Argon卻對她說出令她意想不到的話。

「我說小貓咪呀，你可曾想過，為什麼幾乎所有生物都只有兩只眼睛？」

***

約十個小時前，Silver　Crow以飛行能力將黑雪公主、晶、千百合與拓武等四人帶上東京鐵塔遺址塔頂時，並未把速度加到太快，是以慢慢上升的方式盡可能節省必殺技計量表。

但這次楓子被賦予了同性質的任務，要把黑雪公主、晶、謠等三人帶到中城大樓高樓層，卻從一開始就把背上的疾風推進器開到最大出力燃燒。相信她並不是追求最低油耗，而是以當下分秒必爭的狀況為優先。黑雪公主也認為她的判斷正確，但被人拖著以飛彈般的速度衝向大樓牆壁，實在難免緊張得說話都有點破音。

「喂……喂，Raker，這真的……」

晶立刻接過話頭說：「停得住。」謠又接著說：「嗎？」

楓子聽到這樣的疑問…一

「這個嘛，總會有辦法的吧。」

當她悠哉地答出這句話時，白堊的牆壁已經近在眼前。就在眾人全身緊繃，心想她該不會是想就這麼一頭撞過去的這一瞬間，推進器的噴射就此結束。四人在慣性作用下繼續上升，但勢頭已經迅速減弱，讓她們接下來反而要擔心摔下去，但楓子的目測非常精準。四人就在正好達到拋物線頂點的位置，被拋進了中城大樓外牆上那道寬兩公尺的裂痕之中。

「喝！」

進入大樓內的瞬間，黑雪公主伸出左手，以鋒利的劍尖刺穿外牆的斷面。這一來固然暫時停止下墜，卻讓四人份的重量全都掛在黑雪公主的一隻手上。

「Lotus，你再撐三秒就好！」

楓子這麼一喊，將抓住她右手的晶高高舉起。Aqua　Current失去的大部分水流裝甲仍未恢復，身體似乎也因此而輕巧得多，一抓住被雷射切開的地板邊緣，就輕輕鬆鬆跳了上去。緊接著立刻往下伸出雙手，從楓子手上接過謠，把她拉了上去。

接著楓子也在晶的幫助下爬上地板，黑雪公主擺脫了三人份的體重之後，以插在牆上的劍尖為支點，將自己的身體甩了上去。她在空中翻了個筋斗，落到同伴們身邊。

「你整整用了五秒。」

她指出楓子超過了時間，然而……

「這個世界的五秒，在現實世界中不就只相當於零點零零五秒嗎？別在意別在意。」

楓子說出這種令人似懂非懂的理由後，立刻改變話題。

「別說這些了，不知道這裡是几樓？」

聽她這麼一問，黑雪公主就和晶與謠一起放眼望向四周。這昏暗的空間相當寬闊，還以等間隔設有成排的大理石長桌。

「餐廳……不，應該是辦公宰。中城大樓有五十四樓，我覺得我們差不多是衝進三分之二的高度，所以我想應該是三十五樓左右的辦公樓層……」

黑雪公主這麼回答，晶與謠也點頭表示同意。楓子瞄了天花板一眼，犀利地眯起鏡頭眼。

「也就是說，離我們要去的四十五樓還有十層樓了。如果只差這麼幾層樓，與其找上樓的樓梯，還不如用跳的爬上梅丹佐用雷射轟出來的裂痕上去來得快。」

「也許是這樣沒錯，但如果樓上有人等著，從裂縫現身的我們就會變成上好的活靶……」

「那我們就在離裂縫有點距離的地方，把天花板打出洞來，先放個範圍攻擊之後再衝進去吧。所幸不管怎麼攻擊，都不可能破壞傳送門。」

「說……說得也是。不過……Raker，你從以前就是這種衝鋒隊長的個性嗎？」

楓子在上一代的黑暗星雲裡也是擔任副團長，但黑雪公主在領土戰裡和她並肩作戰的經驗其實不多。因為每周都必須同時防守多個戰區，讓她們兩人往往得分頭指揮不同團隊來應戰。

對於黑雪公主的疑問，楓子只以平靜的微笑回應，但在領土戰中總是和楓子搭檔的謠則雙屑顫抖著說：

「我想不衝鋒的人，應該不會被人取個ICBM之類的外號。」

「原來如此，這我可想通了——那這次我們也一路直搗黃龍吧。Maiden，幫我一把。」

黑雪公主仰望著天花板這麼一說，巫女型虛擬角色立刻換了個人似的用力點頭答應：

「了解！」

在她們四人當中，當然就屬Ardor　Maiden擁有最強大的遠距離攻擊力，但她拿手的火焰攻擊在貫穿力上並不如物理攻擊。每貫穿一層天花板，威力就會往水平方向擴散，也許根本貫穿不到四十五樓。當然只要連續發射兩三次，遲早總會貫穿，但這只是在浪費必殺技計量表。

於是黑雪公主決定由自己來開路，右手劍垂直擺好架式。謠在她身旁將火焰箭搭上長弓，同樣瞄準天花板。黑雪公主確定楓子與晶退開了幾步之後，這才凝聚想像。

「我要動手了……超頻驅動！紅色模式！」

Black　Lotus這麼一喊，行遍全身的接縫線就閃出明亮的火紅色光芒，顯示她將虛擬角色的能力平衡調整為適合遠距離攻擊。紅光一路延伸到她的右手上，凝聚在劍尖發出高亢的震動聲響。

「————『奪命擊』！」

她喊出招式名稱，右手猛然往上一伸。

這招在多年前由師父傳授的心念攻擊，接連貫穿大理石的天花板，發出一聲又一聲硬質的聲響。中城大樓每一層樓的高度約有四點五公尺，所以十層樓就是四十五公尺。這樣的距離接近這一招的最大射程，但她非打到不可。黑雪公主卯足所有的想像力，讓火紅的長槍不斷往前延伸。當刺穿天花板的感覺來到第八次、第九——第十次的瞬間，她往後一倒似的退了開去。

看到黑雪公主腳步踉蹌，楓子立刻上前扶住她的雙肩。幾乎就在同時，謠對準天花板開出的洞拉滿了長弓。

「『火焰漩渦』!」

她以堅毅的嗓音喊出的招式名稱，不是心念攻擊而是必殺技，但魄力卻超乎「奪命擊」之上。搭在弓上的火焰箭瞬間巨大化，箭頭猛然開始旋轉。整枝箭化為一道火焰漩渦，一路灑出大量的光與熱發射出去。

黑雪公主的劍在天花板上打出的洞直徑約有五十公分，而火焰螺旋一路竄升，更將洞口的直徑劈開到將近兩倍大。若說Maiden在對青龍一戰中使用的「火焰暴雨」是特化範圍攻擊的招式，這招「火焰漩渦」就是追求直線前進的招式。即使是從大海空間的海水中發射，威力也足以一路蒸發海水，前進數十公尺之遠。由於火焰沒有實體，對岩石或金屬的屏障缺乏貫穿力，但只要開出一個小小的洞，就能從洞口貫穿，一路灌進深處——

一聲爆響從頭上遠處傳來。是火焰螺旋沿著奪命擊開出的軌道前進，抵達了四十五樓而爆炸。即使有人在裂縫附近埋伏，受到這來自背後的範圍攻擊，即使並未當場死亡，應該也已經受到重創。

「Maiden，有沒有增加點數？」

黑雪公主立刻發問，謠維持長弓瞄向正上方的姿勢搖了搖頭。

「沒有……可是，有打到的感覺！」

「好，我們一口氣衝過去！大家跟我來！」

黑雪公主大喊一聲，來到大涮的正下方全力一跳。即使沒有特別突出的跳躍能力，輕量級的高等級玩家只靠虛擬角色的基礎能力，也能夠跳到一層樓的高度。

黑雪公主從邊緣還燒得火紅的大洞穿出，在上方樓層的地板上落地，楓子、晶與謠也都依序從洞口跳了上來。四人馬不停蹄地持續跳躍，沿著在中城大樓中開出的臨時坑道垂直上升。

隨著她們要去的四十五樓愈來愈近，黑雪公主也開始感受到虛擬角色的裝甲表面有種帶電似的麻刺感。

那是一種確切的預感。很類似與當初衝進四神青龍的領域時，又或者是和大天使梅丹佐的本體對峙時的感覺，但又不太一樣。這和系統上的戰鬥力無關，是一種感受得到去路上有著散發濃縮惡意者屏息以待的感覺。

但無論有著什麼樣的威脅等著她們，她們都不可能選擇退縮。

遭到Black　Vice擄走的紅之王Scarlet　Rain-U子，是出於義氣才出力幫助黑之團，試圖拯救Sky　Raker的「下輩」，同時也是Silver　Crow的好對手Ash　Roller。而且最重要的是，仁子對黑雪公主來說也已經是個非常重要的朋友。

兩年十個月前，黑雪公主拋下所有人情牽絆，任由黑暗星雲瓦解。儘管現在「四大元素」之中已經有三個人回到軍團之中，但當時的大多數團員至今仍未出現在杉並戰區。

那也難怪，畢竟黑雪公主曾經兩度辜負了他們。

第一次是任憑激情驅使，砍下初代紅之王的首級，讓黑暗星雲與其他六大軍團完全敵對。

第二次則是在禁城攻略戰以淒慘敗退收場之後，不試圖重建軍團，就逃離了加速世界。

只要黑雪公主有著堅定的意志，相信即使處在那種艱難的狀況下，依然有可能集合黑暗星雲的團員，至少勉強維持住當時大本營所在的杉並第一戰區，並試圖救出Ardor　Maiden，Aqua　Current與Graphite　Edge。但黑雪公主並未這麼做。她拋下在四神領域逃脫過程中喪失大量點數的團員，把自己關在封閉式網路當中長達兩年似上。

將黑雪公主從這種只顧舔著自己傷口的停滯時間中拉出來的，是一隻有著白銀翅膀的小烏鴉。黑雪公主不知道多少次得到這個「下輩」的鼓勵、引導與教導。

她再也不會犯下同樣的過錯。再也不會做出拋棄同伴……拋棄朋友的事來。她要救回仁子，一定要。

「……Lotus，下一樓就是四十五樓了！」

完成第九次跳躍後，楓子以尖銳的嗓音提醒，黑雪公主回答：「知道了。」她暫時停下腳步，等隊伍最後的謠追上，然後快速下達指示：

「Raker、Current、Maiden，我們應該放在第一優先的目標，就是從傳送門回到現實世界，拔掉Rain的傳輸線。最先接觸到傳送門的人就直接脫離，剩下三人繼續搜索，以進行第二目標……破壞ISS套件本體。遇到礙事的人就毫不留情地打倒，對動用心念也不要猶豫。」

三人強而有力地點點頭後，黑雪公主也點頭回應。她抬頭望向天花板上斷面終於冷卻下來的大洞，小聲呼喊：

「——我們上！」

黑雪公主沉腰蓄勢，右腳尖端在大理石地板上濺出耀眼的火花，實施了第十次跳躍。

***

「生物只有兩只眼睛……的理由……？」

美早小聲復誦了Argon　Array這個唐突的問題。

現在不是在這種地方進行生物學問答的時候了，但被Argon可怕的全方位心念攻擊「無限陣列」打穿全身多處所帶來的劇痛，到現在仍然肆虐著她的虛擬神經系統，讓她暫時無法正常行動。先不說能不能行動，要是對方再用一次同樣的招式，剩下的少許體力計量表就會瞬間耗盡。

Argon不對美早施加致命一擊，反而想聊這些沒什麼意義的話題，看不出她這麼做的意圖何在。但現在——至少在疼痛稍微和緩之前，也只能將計就計了。

「……是進化過程中做出最佳化的結果。」

美早說出最常識性的答案，Argon似乎早有料到，在大型護目鏡下露出甜笑。

「這當然是沒錯啦……你知道嗎？我們脊椎動物的祖先呢，在水底下生活的時候，頭頂有著第三只眼睛呢。說是叫作『顱頂眼』。」

「…………」

分析者對美早的沉默顯得並不在意，饒舌地說下去：

「我們的腦子裡，也留下了這第三只眼睛存在過的痕跡。叫作松果體，你應該也聽過吧？這個東西啊，原本是我們的第三只眼睛呢。似乎是我們的祖先從水中來到陸地上的時候退化掉的，但對理由好像就有很多種學說了。可是我是這麼想的，脊椎動物的眼睛啊，有三只都已經太多了……因為眼球這種裝置啊，性能實在太高了。」

「……性能太高……？」

「對。要把眼球裡的視網膜捕捉到的光，重新建構成我們可以理解的影像，對大腦來說是非常繁重的處理工作。繁重到光處理兩只眼睛，就已經竭盡全力。而我們實際上可以看得清楚的，不也就只有視野正中央，也就是視線集中的部分嗎？」

怎麼想都不覺得這種話題應該在無限制空間，何況還是在戰鬥中談論，但美早還是忍不住回答：

「也不是只有眼睛這樣。耳朵也只聽得見注意去聽的聲響，滋味和氣味也是一樣。」

「說得也是。可是啊，耳朵感覺的是空氣分子的震動，舌頭和鼻子捕捉到的，也都是各種分子的滋味和氣味，不是嗎？但是就只有眼睛偵測到的，是光這種粒子。雖然不確定是波還是粒子，不過比起分子可特別多了。小貓咪，你知道嗎？光子這種東西的大小，是沒有辦法定義的。而我們的眼睛，看的就是這麼神奇的東西。」

「就算沒有大小可言，還是有能量。」

聽到美早的反駁，Argon再度露出甜笑，彈響右手手指。

「沒錯，這就是重點。光射進我們眼睛，能量會被視網膜的視覺細胞吸收，轉換成電子能量沿著視神經傳遞到大腦的視覺領域，處理成我們可以認知的影像……也就是說，最終這些光子都會消失。像聽覺和味覺，都和這種情形不一樣。空氣分子不會在耳膜消失，滋味和氣味的分子頂多也只會被分解不會消失，不是嗎？」

美早注意到分析者平常說話聲調總顯得頗為活潑，不知不覺間卻開始變得低沉而冰冷。Argon以像是看著可怕事物的眼神，低頭朝全身裝甲上整齊排列的鏡頭——眼睛——瞥了一眼，繼續說下去：

「——可是啊，跑進眼睛的光子會消失。我們的眼睛啊……會吃掉光。這麼可怕的東西，兩只都已經太多了。」

「結果你到底想說什麼？」

美早讓疼痛總算開始淡去的野獸身軀慢慢壓低，準備進行跳躍，同時問出這句話。

「說得也是，要說我為什麼講這種事情講半天……」

Argon張開纖細的雙手，輕輕聳了聳肩膀回答：

「我只是在爭取時間——『眼花撩亂』（Razzle Dazzle）。」

當Argon念出招式名稱的前半段，美早已經緊緊閉上雙眼，猛然蹬地而起。關於Argon　Array這項必殺技的情報，她已經透過黑之團得知。這是一種從頭上的四連裝鏡頭發出強烈的光芒，藉以擾亂敵人視覺的障眼法。光本身沒有攻擊力。既然知道這一點，那就是搶攻的好機會。

美早朝半秒鐘前Argon所在的位置揮下右手，有如刀刃般鋒利的鈎爪擦過堅硬的裝甲……

——太淺了！

美早仍然閉著眼睛，接著又揮出左手。既然Argon在爭取到時間後反而動用障眼法類的招式，肯定表示她的心念攻擊「無限陣列」無法連續發動。雖然不知道不需消耗殺技計量表的心念攻擊之所以無法連續發動的理由，但現在只要知道這個事實就夠了。Argon的體力計量表也和美甲一樣所剩無幾，只要再咬到一口就能打倒她。

但左手也同樣只淺淺劃過敵人的裝甲。

Argon的聲息逐漸遠去。萬萬不能被她給跑了。美早無可奈何地睜開眼睛一看，結果炫光雖已減弱，仍有媲美閃光手榴彈似的白光刺進雙眼。美早在這炫光後頭，看見了淡淡的灰色影子轉過身去。

「嘎嗚！」

美早一聲咆哮，猛力一跳。

但她雙手鈎爪抓到的，只有平滑的大理石。看樣子先前她所看到的，是Argon映在建築物牆上的影子。脆弱的牆壁承受不住Blood　Leopard的衝鋒而倒塌，讓美早一頭撞進建築物內。

「剛剛的話題我們下次再聊羅，小貓咪。」

含笑的說話嗓音急速遠去。

不能讓她給跑了。拿下Argon以做為對加速研究社的交涉材料是美早的職責。Silver　Crow就是相信Blood　Leopard辦得到，才會把後績的工作托付給她，孤身去追Black　Vice。

哪怕只看了一瞬間，看到「眼花撩亂」閃光的雙眼仍未完全恢復。然而就像Argon自己在先前的談話中所說，人的知覺不是只有視覺。

美早以豹敏銳的聽覺捕捉腳步聲，並以四肢的肉趾捕捉地面的震動，發現Argon似乎是朝北跑開。這和Crow追著Vice而去的方位不同。雖然不知道Argon的目的地，但不管她去哪裡，自己該做的就是追上去。

美早再度穿破牆壁來到道路上之後，就放低姿勢飛奔。仍然白化的視野，分辨不出路邊的柵欄與倒塌的柱子等小型的障礙物，但她一律用頭撞碎不斷前進。由於屬于紅色系，她的裝甲不怎麼厚，但升上8級後基礎防御力和體力都上升許多。如果只有6級，多半中了Argon的全方位雷射時就已經當場斃命。

美早長久以來一直不升級，就是為了從四神青龍的巢穴中救出冰見晶／Aqua　Current。Current是美早的上輩，但同時也是黑暗星雲幹部集團「四大元素」之一。

現在的日珥中，仍有少數團員仍然恨著讓上一代紅之王Red　Rider失去所有點數而退場的黑之王——像在昨天領土戰爭中跑去攻打杉並的Blaze　Heart籌人，就是代表性的人物。而身屬紅之團幹部集團「三獸士」的美早點數已經足以升級，卻為了Current而不升級，也只能說是一種背叛軍團的行為。

但無論是紅之王Scarlet　Rain，還是三獸士當中的另外兩人，都容許了美早的任性。而美早所存下的點數本應在挨了青龍的「等級吸收」後就已消滅，卻又是黑之團的「時鐘魔女」LimeBell幫她救了回來。她是在這麼多人的支持與幫助下才終於到達8級，這個時候不發揮全力，又要留到什麼時候？

「嘎嚕喔喔喔喔！」

美早一邊飛奔，一邊發出也算是半獸人虛擬角色優勢之一的野生咆哮。偏白的視野正中央，浮現出一個紫色的輪廓。等到追上Argon就不要再說廢話，唯一要做的就是瞬間殺了她，把她變成死亡標記。

就在美早後腿蓄足力道，猛力一跳的時候，障眼法的效果終於消失。恢復了視力的雙眼，捕捉到了Argon停下腳步而轉過身來的身影。她全身長出來的鏡頭群已經消失無踪，是放棄逃走了嗎？……不對，不是這樣。

這個苗條的虛擬角色迅速沉入地面。

大型建築物的影子延伸到Argon腳下。仔細一看，以Argon為中心，半徑兩公尺左右的範圍內，影子似乎都化為一種漆黑的液體。這暗色的沼澤已經將Argon的身體吞沒了一半以上。

擁有躲進影子這種能力的，應該不是Argon　Array，而是Black　Vice。雖然怎麼想都不覺得逃往完全不同方向的Vice會在附近，但或#tVice是以別的手段把自己的能力借給了Argon……還是說……？

美早在腦海角落思考這個問題的同時，盡力伸長雙手想阻止Argon逃走。

但鈎爪這次又只在她大大的帽子上淺淺劃過。

分析者嘴邊露出淡淡的微笑，全身沒入影子之中。

——你別想跑！

美早為了跟著跳進影子裡，在著地的同時掉過頭來，毫不猶豫地將雙手伸進漆黑沼澤，就在一陣渾濁而令人不舒服的水聲中，雙手沒入到手肘附近。

但也只有這樣。不知不覺間，影子沼澤的直徑已經縮小到比美早的肩寬還窄。不但虛擬角色的手肘卡在洞口而鑽不進去，持續縮小的洞口更以無從抗拒的強大壓力擠壓她的雙手。

「變形！」

美早喊出語音指令，從豹形變回人形。她想讓虛擬身體變小一頭鑽過，但洞口縮小的速度比她更快，這次是雙肩卡在洞口，讓她進不去。

這多半是Black　Vice事先就在這個座標上創造出來的定時式空間轉移入口。Argon之所以要講那些話來爭取時間，多半是為了配合入口消失的時機。要是讓洞口就這麼關上，追踪手段將就此消失。

「咕……嗚……！」

美早卯足全力，想撐開影子洞口。然而洞口縮小的力量壓倒性地強，雙手裝甲當場龜裂，體力計量表又被削減得更少。

剩下的手段只有一種。就是再度變身成豹，用把自己化為砲彈的必殺技「流血砲擊」衝淮入口。

使用那一招將使自身受到莫大的反作用力損傷，體力計量表所剩無幾，很可能會承受不住。但她別無選擇。就算不用這招，也只會被縮小的洞口扯斷雙臂而死。

「變……」

就在她以沙啞的嗓音正要念出變身指令之際……

背後聽到兩人份的腳步聲與喊聲。

「Pard小姐！」

「LeoPard小姐！」

不用轉頭去看，也知道是黑暗星雲的Lime　Bell與Cyan　Pile。她暫停變身回頭大喊：

「幫我撐開這個洞！」

兩人分別在美早左右兩側停步後，似乎瞬間理解了狀況。Lime　Bell立刻蹲下來，想將雙手伸進洞裡，但Cyan　Pile阻止了她。

「慢著，Bell!　Leopard小姐，這裡請交給我！」

這個大個子的藍色系虛擬角色，將裝備在右手上的打樁機型強化外裝對准入口擺好架式，接著大喊：

「我數到零就請你從洞口退開！三、二、一……」

一旦抽出雙手，入口多半會在幾秒鐘內關閉並消失。但美早揮開剎那間的猶豫，在聽見他喊「零！」的同時跳開一大步。Cyan　Pile踏上一步，來到她讓出來的位置，喊出了她陌生的招式名稱。

「——『螺旋重力錘』!」

就在強化外裝砲口內藏的鐵樁收納進去的同時，砲管應聲擴大。一陣藍色閃光中射出來的，是一根直徑達到鐵樁兩倍以上的電動錘鑽。劇烈旋轉的鋼柱被夾在即將關上的入口，發出異樣的聲響而停止。

但這陣寂靜立刻被打破。錘鑽的出力壓過入口的壓力，一邊濺出大量的火花，一邊開始旋轉。隨著鑽頭愈鑽愈深，洞口周圍也迸出放射狀的裂痕。

「喔……喔喔喔……」

Cyan　Pile大聲吼叫的同時再加把勁，右手猛力往下一壓。一陣像是空間本身遭到破壞似的異樣巨響響起，入口的邊緣粉碎四散。

洞口被這一下破開到直徑將近有兩公尺之寬，裡頭充滿了黏液狀的黑暗。Cyan　Pile用力過猛，整個人眼看就要跌了進去，美早抓住他的雙肩，大喊：

「GJ！剩下就交給我！」

說著朝洞口縱身一跳。就在液體化的黑暗淹沒到胸口時，Pile和Bell相視點頭，跟著美早跳了下去。

雖然不知道這入口通往何處，但既然Argon會試圖防止他們追踪，就很可能是通往加速研究社的重要據點。那兒的危險比起中城大樓，多半是有過之而無不及。

但美早尚未說話，Lime　Bell就毅然喊道：

「我們也一起去！誰叫我們……」

Bell說到這裡，美早的頭已經完全被黑暗吞沒，再也聽不見她說什麼。但美早用心靈的耳朵，聽到了「是同伴」這後半句話。

影子隧道開始將三名入侵者衝往別處。視野被淹沒成全黑，聽覺也完全被堵住。即使伸出手去，手指也摸不到任何東西。美早也只能任由急流衝走，祈禱不要和Pile他們分開，祈禱還來得及追上Argon。

當然，也要祈禱能夠順利救出紅之王Scarlet　Rain……救出仁子。

不，只祈禱是不夠的。她必須卯足所有的智慧與力量，將願望化為現實。

美早縮起身體，任由這條沒有光的水路將自己帶往他方，並在心中強而有力地發下誓言。

***

東京中城大樓四十五樓。

在現實世界中，這裡應該是超高級大飯店的大廳。而加速世界似乎也反映出建築物的原有構造。當黑雪公主等人從地板洞口衝進來，一片有著方形柱子排列得井然有序的寬廣空間就映入她們的眼簾。

迅速察看完地形的同時，將意識切換到搜敵模式。這個樓層的光線很昏暗，四周的牆壁部落在陰影中，但至少看得見的範圍內並沒有任何在動的物體。然而從樓下發射「火焰漩渦」的謠說「有打到的感覺」，所以肯定有人潛伏在這個樓層。

用遠程攻擊打中目標時那種「打到的感覺」，在現實世界中也許屬於比較超自然的第六感，但在這個世界裡卻有著確切的根據。只要遠距離攻擊命中公敵或對站虛擬角色，哪怕是在視野外命中，必殺技計量表都會增加，而且增加的量與破壞地形物件時有著相當明顯的差距，謠這樣的老手不可能看錯。

而謠跟在楓子與晶後頭，從地板的洞口跳上來時，立刻就把火焰箭搭上了左手的長弓備戰。她多半是預料到一上來就會發生戰鬥，但注意到並未有敵人出現，有些困惑地輕聲說：

「……我從樓下放箭的時候，很肯定有傷到東西……」

「你的點數沒增加，對吧？」

楓子小聲一問，巫女型虛擬角色就點點頭回答：

「是。這人不是受到損傷就撤退……就是……」

「躲在暗處。」

晶接下這個推論，讓水藍色的鏡頭眼迅速往前後左右掃動。但就連敏銳的她，似乎也找不出敵人。

黑雪公主思索了一瞬間後，對她們三人說：

「沒關係，我們的最優先目標是迅速脫離。管他有沒有埋伏，衝進傳送門就對了。」

「我贊成。可是，有一個問題。」

「Current，什麼問題？」

「該有傳送門的地方，沒有傳送門。」

黑雪公主沒料到Aqua　Current會說出這句話，盯著她的臉細看。以狀似水晶的透明材質構成的面具朝向樓層南方。

「以前我曾經從這裡離開好幾次，所以不會弄錯。中城大樓的傳送門，應該就位於四十五樓南側牆壁的牆邊。」

黑雪公主和謠與楓子同時順著晶的視線望去，凝視五十公尺前方的暗處。傳送門理應會發出特有的持續脈動式藍色光芒，但她們連反射的光都看不見。吸引住她們目光的，是一道直線橫過樓層正中央的焦黑裂縫。

「難道……梅丹佐的雷射被反射回去，破壞了傳送門……？」

聽到楓子的自言自語，黑雪公主由於震驚過度，反駁時忍不住加大了音量。

「不可能！無限制空間的傳送門不可能破壞，也不可能移動。哪怕整棟建築物都被破壞，傳送門應該也會留在固定座標！」

「我也這麼覺得，可是……」

就在這時——

晶一直在凝視原本設置傳送門的位置，這時忽然尖銳地低聲喊道：

「慢著。有東西……那裡有東西。」

「……有東西……？」

黑雪公主與楓子同時再度望向樓層南方，把所有注意力集中在雙眼。視野中的對比度上升，讓先前融入濃濃黑暗中的物體慢慢浮現出輪廓。

這個物體很大，全長將近三公尺。形狀看似球體，但從這個距離也看不出別的跡象。

「……Maiden，麻煩你朝那一帶的牆壁發射火焰箭。」

謠點頭回應黑雪公主的指示，舉起了長弓。往斜上方發射出去的火焰箭劃出一道弧線往前飛，穿刺在南側牆壁的高處，橘色的光芒驅開了黑暗。

「那……那是……什麼東西……？」

以沙啞嗓音發出驚呼的是楓子。其餘三人連聲音都發不出來，只震驚得瞪大眼睛。

這個物體穩穩盤踞在斷開樓層的裂縫右方將近十公尺的地板上。

黑雪公主最先想到的詞彙是「腦髓」。整個巨大的球狀物件表面，有著迷宮般錯綜複雜的凹凸紋路。這些爬遍整個球面的細小網狀紋路不停地脈動，令人無法不去聯想到生物……不，應該說是無法不聯想到人類的腦髓。

但球體的顏色是吸收所有光線的消光黑，前方有著一道橫向的深層裂痕。如果是仿人腦，應該不是分成上下，而是分成左腦與右腦。而這樣的差異更讓這個物件令人毛骨悚然，讓人感覺它彷佛是一種很類似人類，卻有著決定性差異生物的大腦——

想到這裡，黑雪公主才總算注意到。

潛伏在黑暗中脈動的巨大大腦。這個概念早已存在於黑雪公主心中。雖然她並未直接看到，卻已經從Silver　Crow與Lime　Bell口中聽過詳細的報告。也就是關於他們說在「BRAIN　BURST中央伺服器」中遭遇到的，蔓延於加速世界當中的黑暗之力根源。

「……難道說，那就是………」

黑雪公主以幾乎不成聲的嗓音說到這裡，楓子就接著說：

「………SS套件的本體……？」

本以為巧妙隱藏在中城大樓巨大空間之中的最終目標——雖然現在的優先順位已經降到第二——竟然就這麼毫不設防地設置，不，應該說是棄置在這裡，讓她們一時間難以置信。認為這是敵人用來引入侵者掉進圈套而設置的冒牌貨，應該是比較合理的推測。但黑雪公主的視覺與直覺，都強烈地告訴她這就是真正的套件本體。若這巨大腦髓是隨便拼湊起來的物件，絕對無法滲流出這種妖氣般的沉重壓力。

晶與謠以沙啞的嗓音對黑雪公主的直覺表示贊同。

「……我想，應該是真貨。」

「我的感覺也是這樣……」

「……唔……」

黑雪公主點點頭，先將震驚擺到一旁，迅速運轉思緒。

如果這個巨大腦髓就是ISS套件的本體，就應該迅速加以破壞。這樣一來，寄生在Ash　Roller以及其他許多超頻連線者身上的套件終端機就會全部消滅，當前的危機也將離開加速世界。這正是黑暗星雲與日珥組成聯軍進行的連續任務所要達成的最終目標。

但相對的，黑雪公主等人又必須分秒必爭地去到傳送門，在現實世界拔掉接在仁子神經連結裝置上的傳輸線。即使成功破壞套件本體，若代價是導致紅之王損失所有點數，紅黑兩軍團多半將會受到更甚於ISS套件蔓延到整個加速世界之上的毀滅性打擊。不管是從內或從外兩種觀點都不例外。

如果晶說得沒錯，中城大樓的傳送門已經消失，那麼她們就應該立刻離開大樓，前往第二近的登出點所在的六本木山莊大樓。但如果她們選擇離開，下次再回到這裡時，ISS套件還會像現在這樣毫不設防嗎？雖然也可以先試著攻擊，觀察是否能在短時間內破壞，但怎麼想都不覺得有辦法看到這個物件的體力計量表，而且這一擊也可能導致事態往她們意想不到的方向演變。

黑雪公主面臨的這個二選一難題是不容犯錯的，相信其餘三人也感受得到她的苦惱。楓子走向她身邊，以迅速但平靜的語調輕聲說道：

「小幸的選擇，就是我們的選擇。不管造成什麼樣的後果，這一切我們都會和你一起扛下去。」

晶與謠也都從鏡頭眼上發出堅定不移的光芒，深深點頭。

黑雪公主點頭回應，對信賴的同伴們說出了她的選擇：

「我們立刻前往六本木山莊大樓。直線距離是七百公尺，傳送門位於四十九樓。我們要在五分鐘內抵達。」

「「「了解！」」」

三人應答的聲音推了黑雪公主一把，讓她往南邁出腳步。六本木山莊大樓位於東京中城大樓的西南方，與其先下到地上，多半還不如請楓子用能量應該已經恢復到一定程度的疾風推進器載著大家能飛多遠就飛多遠。

四人開始沿著梅丹佐的雷射在大理石地板上劃出的南北向裂痕奔跑。

就在她們在五十公尺寬的樓層中跑過一半左右的時候—

盤踞在去路右側的巨大腦髓發生了變化。

「蓮姊！」

聽到跑在最後面的謠大喊，黑雪公主反射性地轉頭一看，結果看見——

從漆黑的大腦前方水平劃過的裂痕，慢慢往上下張開。

起初她以為這個大腦不是分成左腦與右腦，而是「上腦」與「下腦」。但看樣子只有腦的表而在動，錯綜複雜的凹凸紋路擠得起了縐折而疊起，讓藏在內部的東西慢慢露了出來。

「不要停下腳步，直接跑過去！」

黑雪公主呼喊之餘，卻也沒辦法將視線從腦髓上移開。

令人意外的是，往上下滑開的外膜內側是有著光澤的曲面。看來這個球體的質感類似沾濕的玻璃，只是外層包著一層腦髓。露出部分的中央呈鏡頭狀隆起，從內部發出朦朧的光芒。

這個直徑怕不有一公尺半的鏡頭部分忽然動了。

腦髓內部的整個玻璃球體往上下左右轉動，動作非常有生物的感覺，讓人產生一種難以言喻的嫌惡感。

沒過多久，鏡頭大大往左一轉，眾焦在奔跑的四個人身上。

這一瞬間，黑雪公主察覺到了。

那個大型物件不是腦髓，而是眼球。橫向的裂縫也不是大腦的縱裂，而是眼瞼。露出的玻璃質球體是眼白，正圓形的鏡頭則是眼黑。巨大的眼球蘊含著某種意志，看著黑雪公主等人。

相當於眼黑的鏡頭正中央，存在著像是爬虫類會有的縱向瞳孔，由內而外滲出搖曳的藍色光芒。相較於醜惡到了極點的眼球造型，內部滲出的光芒卻格外清澈，令人覺得眼熟——一種會引發鄉愁般情懷的清澈藍色。

「慢著。」

發出輕聲呼喊的是Aqua　Current。她很少發出這麼僵硬的嗓音，讓黑雪公主、楓子與謠都停下了腳步。

「Current，你怎麼了？」

晶並未立刻回答楓子的疑問，視線一直對在巨大眼球上。幾秒鐘後，她以更迫切的嗓音說：

「……那個眼球裡面……有傳送門。」

「咦咦！」

「這……！」

楓子與謠同時驚呼出聲。黑雪公主屏住氣息，再度仔細觀察鏡頭內側。將這呈現周期性脈動的聲響與光芒，與過去無數次跳進傳送門時留下的印象比對。

色彩、搖動情形、大小。一切都與記憶中的模樣分毫不差。

「……是真的……那是傳送門的光！」

謠以細小的聲音呼喊。黑雪公主，還有楓子，多半也都找不出足以否定她這句話的根據。

「可……可是……用物件包住絕對不容侵犯的傳送門，真的可能發生這種情形嗎……？」

楓子在黑雪公主茫然問出的這個問題上，加上了自己的疑問：

「真要說起來……這ISS套件本體，在系統上到底屬於哪個分類……？從形狀來看，多半不是對戰虛擬角色或公敵，但如果是強化外裝等各種物品，應該就會在變遷的同時消失才對吧……？」

聽她這麼一說，就覺得的確沒錯。假設這個漆黑的巨大腦髓狀眼球型物件是加速研究社打造出來的，然後就這麼放置在無限制空間中，那麼等到切換空間屬性的變遷來臨，物件應該就會被強制消除。若想避免這種情形，就必須在察覺到變遷發生的瞬間，將物件收回物雕魷內，等屬性變遷結束後再讓物件實體化。而且這種操作必須半永久性地持續進行。算來每七天就至少必須進行一次——現實世界則是每十分鐘一次——考慮到變遷發生的頻率，這麼繁複的維護工作實際上是不可能持續下去的。

眼下的狀況充滿了疑點，然而……有一件事她能夠立即斷定。

「……無論這眼球屬於哪一種物品分類，既然傳送門就位在眼球裡面，那我們就必須改變方針。」

黑雪公主將震驚與困惑拋到意識之外，以蘊含力道的聲調說：

「我們現在就立刻破壞這個噁心的眼球……破壞ISS套件本體，然後從露出的傳送門離開無限制空間。Raker、Current、Maiden……」

她雙手劍高聲一划，喊道：

「這裡就是我們的決戰戰場！準備攻擊！」

三人再度回答：「了解！」答話的聲音有著兩倍於先前的決心，驅開了整個樓層的黑暗。

她們組成由黑雪公主領頭，楓子在右、晶在左、謠殿後的隊形，與盤踞在二十公尺前方的巨大眼球對峙。玻璃質感的眼珠仍然從縱向開口的瞳孔內滲出搖曳的藍光，以不合絲毫情緒的無機視線回望她們四人。

不對。

眼球忽然間微微眯起上下眼瞼——它笑了。至少看起來是這樣。

影子般的波動從直徑長達三公尺的巨大身軀中迸射而出。一接觸到這陣波動的瞬間，黑雪公主立刻懂了。眼球的內部充滿了龐大的惡意。追求破壞、哀嚎與殺戮的慾望，化為黑濁的液體充滿在眼球之中，幾乎隨時都會噴射而出。

從瞳孔滲出的傳送門藍光，突然轉化為血一般深黑的紅色。

包裹住眼球的腦髓組織表面，在一陣啵啵聲中冒出了許多顆瘤。這些像潰瘍的隆起立刻像氣球一樣膨脹，然後一齊炸開，從內部噴出黏液與奇怪的物體。

那是一個個約有二十公分大的小型眼球。瞳孔和本體一樣發出渾濁的紅色光芒，下半部長出幾根細長的腳。這些眼球一落到地上，就以細長的腳迅速爬來爬去，模樣像是某種昆虫，數量多半超過十只。

「我……我剛才用『火焰漩渦』燒到的大概就是這個！」

謠舉起長弓瞄準的同時，發出有些破音的說話聲。她的信條是對所有生物都要抱持敬意，但對這種長腳的眼球似乎還是掩飾不住厭惡。而且嚴格說來，這些小型眼球並不是加速世界的小型生物。

「小心點，這些傢伙多半就是ISS套件的終端機！一旦被它們碰到，就有被寄生的危險，要在被接近之前就全部破壞！」

黑雪公主的指示成了導火線，先前只在套件本體四周胡亂竄來竄去的小型眼球，開始一起朝她們四人跑去。

謠接連鳴響長弓的弦。射出的火焰箭都確實射穿套件終端機，讓它們著火，但弓箭的連射速度終究有其極限。當第四只眼球著火時，已經有加倍的眼球跳過這些火焰，張開細得像針的腳飛撲而來。

黑雪公主舉起右腳劍，瞄準最左邊的眼球後大喊：

「『死亡彈幕』！」

她以快得連自己也看不清楚的速度，以右腳發出散彈般的「散踢」。眼球一觸到這每秒高達一百發的連擊效果範圍，就灑出紅光爆裂四散。

黑雪公主以插在地上的左腳劍尖為支點，讓身體往右旋轉。連擊的風暴拖出灰色的殘影流動，接連轟掉撲過來的眼球。ISS套件以壓倒性的力量讓加速世界陷入一片混亂，但終端機本身似乎無法使出遠近兩種心念攻擊，除了試圖以觸手接觸虛擬角色而寄生以外，別無其他攻擊方法。

黑雪公主以必殺技破壞第七個眼球，楓子也以右腳尖銳的鞋跟插穿最後一個眼球。SkyRaker的拿手招式是行雲流水般的掌擊，但就連「鐵腕」似乎也不想用空手打爛這些眼球。

儘管本體產生出來的一打眼球在不到十秒鐘之內就全數遭到殲滅，但說起來在系統上單純只是強化外裝的ISS套件會這樣擅自展開行動，這件事本身就令人難以置信。雖然也有像「災禍之鎧」這種強化外裝本身就擁有意志的案例，但就連那可怕的鎧甲應該也無法獨自活動，一定需要有宿主。

ISS套件到底是什麼東西？加速研究社到底是用什麼樣的手段創造出了這種東西？

黑雪公主的心思差點又困在疑問之中，這才猛然回過神來大喊：

「趁新的一批眼球出現前就毀掉本體！所有人準備全力攻擊！」

這種情形下的全力，意味的是全力動用心念系統的最大火力攻擊。

在無限制空間貿然動用心念會引來大型公敵，但既然是在高層大樓的四十五樓，基本上就不必擔心這種情形。另外若對超頻連線者動用心念攻擊，就有可能迷上以壓倒性力量逼迫敵人屈服的萬能感，掉進「心念的黑暗面」，但既然目標是沒有靈魂的眼球，這方面的問題也就不大。

四人各以不同顏色的過剩光，將大理石地板染上各種顏色，同時算準讓攻擊齊發的時機。

動手。

就在黑雪公主深深吸一口氣，準備喊出這句話的瞬間——

空曠的樓層內，傳來一個男性型對戰虛擬角色顯得有些悠哉的說話嗓音。

「你還是一樣毫不留情。看到你跟那個時候沒有半點兩樣，我可不知道有多高興啊。」

「……是誰！」

黑雪公主並未下達開始攻擊的號令，轉而喝問這人是誰。這個在現實世界中是飯店大廳的樓層裡，有著無數根巨大的柱子。不但多得是地方可以躲，而且聲音回蕩的方向十分複雜，很難從聲音來源掌握到位置。

但黑雪公主還是感覺到了。感覺到聲音是發自ISS套件本體。

她的直覺只對了一半。

一個對戰虛擬角色踩出喀喀作響的堅硬腳步聲，從巨大眼球後方走了出來。

先前那種套件的終端機在本體周圍四面八方竄來竄去。這個說話的人當然應該也位於終端機的反應範圍內，但卻沒受到它們攻擊，唯一的可能就是這人是已經裝備了終端機的ISS套件使用者——也就是敵人。那麼她們就應該立刻以全力攻擊，將他連人帶套件本體一起破壞。

然而一看到從眼球後方現身的虛擬角色所露出的一隻腳，這一瞬間，理智所下的這個判斷立刻被拋諸腦後。

這只腳上有著長靴型的裝甲。

腳跟處延伸出尖銳的馬刺。

而為這些配件賦予的色彩，是純粹得無從比喻的——

紅色。

「…………這……怎麼可能？」

說這句話的是楓子、是晶，還是謠？黑雪公主也試著從喉嚨擠出這句話，但虛擬角色的嘴完全僵住不動。

喀、喀、喀。

附有馬刺的長靴，又踩響地板三次之後停了下來。

這個男性型虛擬角色把左肩靠上ISS套件本體的腦髓狀外皮，右手掀起頭上牛仔帽的帽檐打了聲招呼。

「喲，好久不見啦，Lotus。」

黑雪公主在徹底麻木的意識中，聽見自己的嘴發出破裂的嗓音。

「…………紅之王…………Red　Rider…………」
