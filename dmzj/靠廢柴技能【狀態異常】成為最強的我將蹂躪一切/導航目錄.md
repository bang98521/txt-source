# CONTENTS

靠廢柴技能【狀態異常】成為最強的我將蹂躪一切  
ハズレ枠の【状態異常スキル】で最強になった俺がすべてを蹂躙するまで  
靠废柴技能【状态异常】成为最强的我将蹂躏一切  

作者： 篠崎芳  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/dmzj/out/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/dmzj/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/dmzj/靠廢柴技能【狀態異常】成為最強的我將蹂躪一切/導航目錄.md "導航目錄")




## [第一卷](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7)

- [序章](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00020_%E5%BA%8F%E7%AB%A0.txt)
- [1.告知召喚始末的女神](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00030_1.%E5%91%8A%E7%9F%A5%E5%8F%AC%E5%96%9A%E5%A7%8B%E6%9C%AB%E7%9A%84%E5%A5%B3%E7%A5%9E.txt)
- [2.廢棄遺跡](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00040_2.%E5%BB%A2%E6%A3%84%E9%81%BA%E8%B7%A1.txt)
- [3.通往蹂躪一切的道路](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00050_3.%E9%80%9A%E5%BE%80%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87%E7%9A%84%E9%81%93%E8%B7%AF.txt)
- [4.SOUL EATER](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00060_4.SOUL%20EATER.txt)
- [5.AVENGER（S）](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00070_5.AVENGER%EF%BC%88S%EF%BC%89.txt)
- [6.邂逅](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00080_6.%E9%82%82%E9%80%85.txt)
- [後記](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00090_%E5%BE%8C%E8%A8%98.txt)
- [特典 要我以這身裝扮戰鬥嗎？](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00100_%E7%89%B9%E5%85%B8%20%E8%A6%81%E6%88%91%E4%BB%A5%E9%80%99%E8%BA%AB%E8%A3%9D%E6%89%AE%E6%88%B0%E9%AC%A5%E5%97%8E%EF%BC%9F.txt)
- [特典 倉庫裡的女神](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00110_%E7%89%B9%E5%85%B8%20%E5%80%89%E5%BA%AB%E8%A3%A1%E7%9A%84%E5%A5%B3%E7%A5%9E.txt)
- [特典 賺取旅費](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00120_%E7%89%B9%E5%85%B8%20%E8%B3%BA%E5%8F%96%E6%97%85%E8%B2%BB.txt)
- [特典 勇者們的大浴場](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00130_%E7%89%B9%E5%85%B8%20%E5%8B%87%E8%80%85%E5%80%91%E7%9A%84%E5%A4%A7%E6%B5%B4%E5%A0%B4.txt)
- [特典 高雄姊妹的認知](00000_%E7%AC%AC%E4%B8%80%E5%8D%B7/00140_%E7%89%B9%E5%85%B8%20%E9%AB%98%E9%9B%84%E5%A7%8A%E5%A6%B9%E7%9A%84%E8%AA%8D%E7%9F%A5.txt)

