夜色中，車輪轉動，馬匹發出嘶叫聲。

一邊在床上感受著到馬車的搖晃，一邊從喉嚨裡吐出變冷的呼吸。馬車裡堆積的葡萄酒的味道，奇妙地芬芳。
以前的時候根本就沒有坐過馬車，我也變得相當奢侈了啊。說起來，因走了太多路而使得腳失去知覺這樣的事最近也沒發生過呢。

「但是，這樣好嗎？如果大家知道你離開了伽羅亞瑪利亞的話，會引起一場大騷動的吧。」

抱膝坐在黑暗中的那副姿態。從這兒可以看得一清二楚。
長髮隨風飄動，無言地靜坐著。被月光稍稍照射到的她，給人一種神秘感。

紋章教的聖女，瑪蒂婭。毫無疑問，現在是紋章教這場風暴的中心，在大聖教一側的人都瞄準著她的心臟。
處於那樣境地的她，竟然趁著夜色離開了伽羅亞瑪利亞，實在是太不符合常理了。

瑪蒂婭一瞬間眯著眼睛，好像對我的提問感到驚訝，然後發出聲音。

「安說過了吧。為了和精靈結盟，如果要外出的話也只有現在這個時期了。哎呀，不會是沒好好聽吧？」

我知道黑暗中，那雙眼睛正瞪著我。聖女大人的心情好像很差。無言以繼。雖然只是苦於沉默，所以說了幾句話，但好像不是能夠繼續對話的氣氛。
一時間，只剩下馬車奔馳在原野上的響聲以及隨從護衛的紋章教徒們的馬的嘶鳴聲。

───

「我認為現在正是好機會。受到伽羅亞瑪利亞竟然淪陷這個事實的衝擊，諸侯還沒能做出對應的時候，路易斯大人⋯⋯然後是聖女瑪蒂婭大人，希望你們能前往加薩利亞的空中庭園。」

拉爾格・安的話語在酒館中回響。那爽朗的聲音，在剛剛從二樓下來的我的腦海中回響著。頭頂，隱隱作痛。
一邊用手指壓住太陽穴，一邊張開嘴。

「那又是為什麼？對於大聖堂來說，我們即是罪人，即是惡魔。諸侯們無論何時攻擊這裡都不足為奇。」

「大罪人」這種發自自己的言語，讓心臟微微顫動。胸中阿琉珥娜的身影，變得稍微模糊。
現在，伽羅亞瑪利亞就像是地圖上突出來的一塊。紋章教徒或者說我們，稱之為勢力還太弱了。

「正因為如此，敵人以討伐邪教為大義而獲取了爭奪伽羅亞瑪利亞的權利，敵人就像發現了死肉的野獸群一樣涌上來也不奇怪。也正因為如此，才需要尋求與精靈的同盟？」

聽到了我的話語後安得意洋洋地嘟起了嘴。這種舉止果然很孩子氣。

「事情並沒有這麼簡單哦，路易斯大人。確實，伽羅亞瑪利亞失陷了。但是，本來作為伽羅亞瑪利亞統治階層的貴族們大多都逃跑了。」

稍微睜大眼睛，聽著安說話。

一副得意揚揚的神色，那真的是件好事？擁有本來的統治權的人還活著，這更可以作為他們大義並增加他們的正義感。倒不如說，如果有這樣的人在的話，最好把它們處理掉。

當然，以當前的兵力來說確實不允許做出那樣的選擇。
在皺起眉頭的我的身後，芙拉朵發出聲音。

「原來如此。也就是說，瞄準了諸国尋找合適的傀儡的空白啊。雖然很不爽，但是在那之前是不可能大規模出兵的。」

一邊晃動整齊的黑髮，一邊用手指在下巴上點了點。旁邊的卡麗婭也露出了理解的表情。

「安，還有芙拉朵啊。你們明白固然很好。但如果可以的話，還請讓我這樣的凡人能夠理解。戰場的種種暫且不論，統治權什麼的我可完全無法理解啊。伍德和塞雷爾也是一樣。」

就這樣感受到了並不是什麼出生的差距，而是後天積累的知識的差距。心好痛。

「所謂的統治權，是神賜予的，實質性的東西，路易斯。也就是說，即使以大量兵力使這裡再次陷落，之後其他勢力的擁有統治權的人也都擁有再次發動戰爭的權利。」
「也就是說，無論是誰，都熱衷於獲取這裡的權利。」

芙拉朵一邊凝視著我的眼睛一邊這樣說道。

原來如此，我的大腦也可以理解了。
無論哪股勢力，都難以放棄對伽羅亞瑪利亞的渴望。如果可能的話，都想將成為無主地帶的這裡編入自己的勢力圖中吧。

但是，如果那些正當享有統治權的人還活著的話，就另當別論了。雖說如今這裡只是紋章教的小勢力，但出兵多少會受到阻攔。而且城市的堅固程度，並沒有受到影響。
萬一，出兵成功攻陷後，其他勢力卻推舉擁有正當統治權的人為傀儡向伽羅亞瑪利亞伸出手來，那就糟糕了。

這樣想的話，雖然這裡是空白地帶，但是在準備完畢之前，可以說是很難輕易出手的地方。

「但是，處境仍然凶險。所以很需要瑪蒂婭大人親自出馬簽訂盟約。」
「關於這次的事件，我希望能堅定路易斯大人的立場。」

───

理所當然地要求同行的卡麗婭和芙拉朵裹著毯子睡著了，聖女大人也沉默著，在奇妙的寂靜中，慢慢地想起了安的話。

在紋章教看來我的立場非常不穩定。在伽羅亞瑪利亞挽救瑪蒂婭的生命，擊退了作為敵將的赫爾特。與之相對的，犯下了破壊了他們守護的地下神殿，奪走了紋章教徒生命的罪行。

姑且因為瑪蒂婭而赦免了罪行，但是感情並不會輕易消散，即使在內心深處沉睡，一有機會也會馬上露出獠牙。
因此，安希望我能鞏固自己的立場。

在尋求與精靈締結盟約的旅途中取得了功績的話，就沒有人會公開地說出不滿的話了。是的，她是這麼想的吧。但是說實話，我個人覺得和精靈結盟什麼的，並不是什麼正經事。
並且有些讓人無法理解的地方。為什麼安會如此擁護我呢？
不僅考慮到我們的立場，這次也以護衛的名義，和馬車周圍追跑的其他紋章教徒不同，允許在馬車中同行。我不知道為什麼會被如此特殊對待。至今為止從未受到過這樣的優待。

有可能是因為將我，卡麗婭和芙拉朵看做了寶貴的戰力？還是說受到了我與南音絲女士關係的影響？
雖然也曾親密接觸過，但是安不是會被感情左右行動的人吧。雖然語言的節奏讓人感到柔和，但其深處卻貫徹著冷靜。

啊，還有可能是因為聖女瑪蒂婭大人對我們的信任吧。

「──勇者啊，到達之前，有件事想跟你說。」

地面的顏色已經有了一些細微的變化，馬上就要進入精靈的領域了。
那時瑪蒂婭的話語撕裂了寂靜，這個女人為何會稱呼我為勇者？

「是關於伽羅亞瑪利亞那時的事情，你闖進我和敵將之間的時候。」

因為聲音很沉重，不由得有些緊張，到底會被問什麼。
那時我從赫爾特的一擊下，救下了瑪蒂婭。是想對那時的事情表示感謝嗎？聖女大人也存在著人類的情感呢。

瑪蒂婭輕鬆破壊了我那輕鬆自在的內心，張開了嘴。

「忠告於你。希望你不要認為那件事是對我的恩情。事實上我並沒有感謝你──不，我反而在怨恨你。」

瑪蒂婭的話語在腦海中反覆。
我還無法理解那句話。瑪蒂婭像是把該說的話說完了，嘴唇又緊緊地閉上。就像拒絶著我去詢問，不過我也不打算詢問。

原來如此，至少明白了一件事，安之所以如此關心我，不是因為這個女人的信任。

突然，搖晃的程度發生了變化。在奇妙的沉默中，馬車，進入了傳言是精靈的領域加薩利亞周邊的山區。