# novel

- title: 八男って、それはないでしょう！
- title_zh: 八男？別鬧了！
- title_output: 八男？別鬧了！
- author: Ｙ．Ａ
- source: https://ncode.syosetu.com/n8802bq/
- cover: https://images-na.ssl-images-amazon.com/images/I/81IEGu%2BRFtL.jpg
- publisher: syosetu
- date:
- status: 完結

## series

- name: 八男って、それはないでしょう！

## preface


```
平凡な若手商社員である一宮信吾二十五歳は、明日も仕事だと思いながらベッドに入る。だが、目が覚めるとそこは自宅マンションの寝室ではなくて……。
僻地に領地を持つ貧乏貴族の八男という、存在意義さえ怪しい子供に憑依した彼は、幸いにも魔法の才能があったので早くに自立しようと我が道を進む。
家門と領地継承も、内政無双も経験が無いから無理。
魔法で金を稼いで、自由に生きて何が悪いというのか。まあ、結局人の営みで発生する柵（しがらみ）からは逃れられないのはこの世の常として。
これは、そんな若造ヴェンデリン・フォン・ベンノ・バウマイスターの世界なんて救わないお話である。
※２０２０年アニメ放映予定。書籍版第十七巻まで発売中。
コミック版は第六巻まで発売中（コミックウォーカーにて連載しております）。
ドラマＣＤも発売中です。
同じく書籍化された、銭（インチキ）の力で、戦国の世を駆け抜ける。
（七巻まで発売中）と共によろしくお願いします。
```

## tags

- syosetu
- 残酷な描写あり
- 異世界転生
- 転生・憑依
- 異世界
- 魔法
- ハーレム？
- 貴族


# contribute

- mirronight

# options

## downloadOptions

- noFilePadend: true
- filePrefixMode: 1
- startIndex: 0

## syosetu

- txtdownload_id:
- novel_id: n8802bq

## textlayout

- allow_lf2: false

# link

- [narou.nar.jp](https://narou.nar.jp/search.php?text=n8802bq&novel=all&genre=all&new_genre=all&length=0&down=0&up=100)
- [小説情報](https://ncode.syosetu.com/novelview/infotop/ncode/n8802bq/)
- [八男吧](https://tieba.baidu.com/f?kw=八男)
